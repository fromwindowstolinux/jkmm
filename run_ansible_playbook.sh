#!/bin/bash

# Define the lock file
LOCKFILE="/tmp/run_ansible_playbook.lock"

# Function to remove the lock file on exit
cleanup() {
  rm -f "$LOCKFILE"
}

# Check if the lock file exists
if [ -e "$LOCKFILE" ]; then
  echo "Script is already running."
  exit 1
else

  # Create the lock file
  touch "$LOCKFILE"

  # Set a trap to ensure the lock file is removed on script exit
  trap cleanup EXIT

  # Navigate to the directory where the playbook is located
  cd /data/gv_zmd4/

  # Run the Ansible playbook
  ansible-playbook playbook.yml
fi
